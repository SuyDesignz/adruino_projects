//include the DHT libary to be able to use the temperature and humidity sensor
#include <DHT.h>
#define Type DHT11

/*
    A
   ---
F |   | B
  | G |
   ---
E |   | C
  |   |
   ---
    D

    Shift register digit to segment
   0 -> dot 
   1 -> A
   2 -> B 
   3 -> C
   4 -> D
   5 -> E
   6 -> F
   7 -> G
   
  */

#define DIGIT_ON  HIGH
#define DIGIT_OFF  LOW
#define SEGMENT_ON  LOW
#define SEGMENT_OFF HIGH

int pinShiftClock = 2; //pin 2 on the Arduion and pin 11 on the 74HC595N
int pinLatchClock = 4; //pin 4 on the Arduion and pin 12 on the 74HC595N
int pinSerialDataInput = 7; //pin 7 on the Arduion and pin 14 on the 74HC595N

//D1 to D4 are the input pins for the 4 Digit 7 segment display
//while 1-4 ist from left to right so D4 ist the last digit on the display
int D1 = 9;
int D2 = 10;
int D3 = 11;
int D4 = 12;

//pin 13 is the input pin for the sensor data with a DHT11 sensor type
int sensePin = 13;
DHT HT(sensePin , Type);

// the number of the pushbutton pin
int buttonPin = 6;
int buttonState = 0;
int lastButtonState = 0;
//0 -> show temperatur
//1 -> show humidity
//2 -> disable the display (off)
int buttonFlag = 0;

boolean registerPins[8];

float humidity;
float tempC;
float tempF;

//setup vatiables for the project
void setup() {
  pinMode(pinShiftClock, OUTPUT); //SHTP
  pinMode(pinLatchClock, OUTPUT); //STCP
  pinMode(pinSerialDataInput, OUTPUT); //DS 
  pinMode(D1, OUTPUT);  
  pinMode(D2, OUTPUT);  
  pinMode(D3, OUTPUT);  
  pinMode(D4, OUTPUT); 

  pinMode(buttonPin, INPUT); 

  //begin Serial output on 9600
  //only needed for testing reasons
  //Serial.begin(9600);
  //use the sensor 
  HT.begin();
}

//main loop for the programm
void loop() {
  
  buttonState = digitalRead(buttonPin);

  //button chagned the state
  if(buttonState != lastButtonState)
  {
    //if button is pushed so we change the buttonFlag
    if(buttonState == HIGH)
    {
      if(buttonFlag == 0){
        buttonFlag = 1;
      } else{
        buttonFlag = 0;
      }
    }
    lastButtonState = buttonState;
  }
      
  if (buttonFlag == 0) {
    tempC = HT.readTemperature();
    showNumber(tempC);
  } else if(buttonFlag == 1){
    humidity = HT.readHumidity();
    showNumber(humidity);
  } else{
    
  }
}

void showNumber(float temp)
{
    int firstNumber = temp/10;
    int SeccoundNumber = temp - firstNumber*10;
    int decimalPlaces = (temp - SeccoundNumber - firstNumber*10)*10;
    
    lightNumberAtDigit(firstNumber , D2 , false);
    lightNumberAtDigit(SeccoundNumber , D3 , true);
    lightNumberAtDigit(decimalPlaces , D4, false);
}

void lightNumberAtDigit(int number , int digit , boolean dotFlag)
{
    digitalWrite(digit, DIGIT_ON);
    setRegister(number , dotFlag);
    writeRegister();
    setRegister(10 , false);
    writeRegister();
    digitalWrite(digit, DIGIT_OFF);
}

//writes the register array into the shift register and outputs the data to 
//the PAALLEL DATA OUTPUT pins (Qa(pin 15), Qb (pin 1)  - Qh (pin 7) )
//the output pins are connected to the 4 Digit 7 Segment LED Display
void writeRegister()
{
  digitalWrite(pinLatchClock , LOW);

  for(int i = 7; i >=0; i--)
  {
    digitalWrite(pinShiftClock, LOW);
    digitalWrite(pinSerialDataInput, registerPins[i]);
    digitalWrite(pinShiftClock, HIGH);
  }
  digitalWrite(pinLatchClock , HIGH);
}

//set the register array to the needed status for eache segment 
//need the be simplified for better code documentation and readability
void setRegister(int numberToDisplay, boolean isDotDisplayed) {

  if(isDotDisplayed){
    registerPins[0] = SEGMENT_ON;
  }
  else{
    registerPins[0] = SEGMENT_OFF;
  }

  switch (numberToDisplay){

  case 0:
    registerPins[1] = SEGMENT_ON;
    registerPins[2] = SEGMENT_ON;
    registerPins[3] = SEGMENT_ON;
    registerPins[4] = SEGMENT_ON;
    registerPins[5] = SEGMENT_ON;
    registerPins[6] = SEGMENT_ON;
    registerPins[7] = SEGMENT_OFF;
    break;

  case 1:
    registerPins[1] = SEGMENT_OFF;
    registerPins[2] = SEGMENT_ON;
    registerPins[3] = SEGMENT_ON;
    registerPins[4] = SEGMENT_OFF;
    registerPins[5] = SEGMENT_OFF;
    registerPins[6] = SEGMENT_OFF;
    registerPins[7] = SEGMENT_OFF;
    break;

  case 2:
    registerPins[1] = SEGMENT_ON;
    registerPins[2] = SEGMENT_ON;
    registerPins[3] = SEGMENT_OFF;
    registerPins[4] = SEGMENT_ON;
    registerPins[5] = SEGMENT_ON;
    registerPins[6] = SEGMENT_OFF;
    registerPins[7] = SEGMENT_ON;
    break;

  case 3:
    registerPins[1] = SEGMENT_ON;
    registerPins[2] = SEGMENT_ON;
    registerPins[3] = SEGMENT_ON;
    registerPins[4] = SEGMENT_ON;
    registerPins[5] = SEGMENT_OFF;
    registerPins[6] = SEGMENT_OFF;
    registerPins[7] = SEGMENT_ON;
    break;

  case 4:
    registerPins[1] = SEGMENT_OFF;
    registerPins[2] = SEGMENT_ON;
    registerPins[3] = SEGMENT_ON;
    registerPins[4] = SEGMENT_OFF;
    registerPins[5] = SEGMENT_OFF;
    registerPins[6] = SEGMENT_ON;
    registerPins[7] = SEGMENT_ON;
    break;

  case 5:
    registerPins[1] = SEGMENT_ON;
    registerPins[2] = SEGMENT_OFF;
    registerPins[3] = SEGMENT_ON;
    registerPins[4] = SEGMENT_ON;
    registerPins[5] = SEGMENT_OFF;
    registerPins[6] = SEGMENT_ON;
    registerPins[7] = SEGMENT_ON;
    break;

  case 6:
    registerPins[1] = SEGMENT_ON;
    registerPins[2] = SEGMENT_OFF;
    registerPins[3] = SEGMENT_ON;
    registerPins[4] = SEGMENT_ON;
    registerPins[5] = SEGMENT_ON;
    registerPins[6] = SEGMENT_ON;
    registerPins[7] = SEGMENT_ON;
    break;

  case 7:
    registerPins[1] = SEGMENT_ON;
    registerPins[2] = SEGMENT_ON;
    registerPins[3] = SEGMENT_ON;
    registerPins[4] = SEGMENT_OFF;
    registerPins[5] = SEGMENT_OFF;
    registerPins[6] = SEGMENT_OFF;
    registerPins[7] = SEGMENT_OFF;
    break;

  case 8:
    registerPins[1] = SEGMENT_ON;
    registerPins[2] = SEGMENT_ON;
    registerPins[3] = SEGMENT_ON;
    registerPins[4] = SEGMENT_ON;
    registerPins[5] = SEGMENT_ON;
    registerPins[6] = SEGMENT_ON;
    registerPins[7] = SEGMENT_ON;
    break;

  case 9:
    registerPins[1] = SEGMENT_ON;
    registerPins[2] = SEGMENT_ON;
    registerPins[3] = SEGMENT_ON;
    registerPins[4] = SEGMENT_ON;
    registerPins[5] = SEGMENT_OFF;
    registerPins[6] = SEGMENT_ON;
    registerPins[7] = SEGMENT_ON;
    break;

  case 10:
    registerPins[1] = SEGMENT_OFF;
    registerPins[2] = SEGMENT_OFF;
    registerPins[3] = SEGMENT_OFF;
    registerPins[4] = SEGMENT_OFF;
    registerPins[5] = SEGMENT_OFF;
    registerPins[6] = SEGMENT_OFF;
    registerPins[7] = SEGMENT_OFF;
    break;
  }
}
